
import React from 'react';
import 'react-native-gesture-handler';

import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './src/HomeScreen';
//import Expenses from './src/Expenses';
import Connection from './src/Connection';
import Categories from './src/Categories';
import Graphic from './src/Graphic';
import Createaccount from './src/Createaccount';
import Tab from './src/screens/Tab';





const Stack = createStackNavigator();




export default function App() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
        <Stack.Screen
            name="Connection"
            component={Connection}
          />
        <Stack.Screen
            name="Home"
            component={HomeScreen}
          />
          <Stack.Screen
            name="Expenses"
            component={Tab}
          />
           <Stack.Screen
            name="Categories"
            component={Categories}
          />
           <Stack.Screen
            name="Graphic"
            component={Graphic}
          />
           <Stack.Screen
            name="Createaccount"
            component={Createaccount}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
}



                                        
