import React from 'react'
import { FlatList, Text, View } from 'react-native'
import { UserRow } from './UserRow'

const _renderItem = ({ item }) => (
  <UserRow name={item.name}  date={item.date} sum={item.sum} />
);

const _renderSeparator = () => <View style={{ height: 1, backgroundColor: 'grey' }} />


export const UserList = props => 
<FlatList
data={props.data}
renderItem={_renderItem}
keyExtractor={item => String(item.id)}
ItemSeparatorComponent={_renderSeparator}
/>
