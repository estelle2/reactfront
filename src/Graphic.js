import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';

class Graphic extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Graphic!</Text>
        <Button
          title="Back to home"
          onPress={() =>
            this.props.navigation.navigate('Home')
          }
        />
      </View>
    );
  }
}

// ...

export default Graphic;


const styles = StyleSheet.create({
   
  container: {
    flex: 1,
    flexDirection: "column"
  },

});