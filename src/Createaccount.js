import React from 'react';
/*import logo from 'https://i.imgur.com/TkIrScD.png'; */





import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground, TextInput, ScrollView } from 'react-native';

const image = { uri: "https://image.freepik.com/photos-gratuite/plante-provenant-pieces-monnaie-dans-bocal-verre-nature-floue_1150-17704.jpg" };


class Connection extends React.Component {


  render() {
    return (
      <View style={styles.container}>
          <ImageBackground source={image} style={styles.image}>
          <ScrollView style={styles.essai}>

       <View style={styles.formu}>       
       <Text style={styles.header}>S'enregistrer</Text>

       <TextInput style={styles.textinput} placeholder="Your name"/>
       <TextInput style={styles.textinput} placeholder="Your email"/>
       <TextInput style={styles.textinput} placeholder="Your passeword" secureTextEntry={true} />
        
        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]} onPress={() =>
              this.props.navigation.navigate('Connection')
            }>
            <Text style={styles.btnText}>S'inscrire</Text>
        </TouchableOpacity>
        </View>
        </ScrollView>
        </ImageBackground>  
   
    </View>
    )
  }
}
  export default Connection;
  
    
  const styles = StyleSheet.create({
   
    container: {
      flex: 1,
      flexDirection: "column",
      
      
    },

    formu:{
        backgroundColor:"#cbf2b5",
        opacity:0.7,
        
        margin:20,
        padding:50,
    },

    header: {
      color:"green",
      textAlign:"center",
      fontWeight: "bold",
      fontSize: 40,
      paddingBottom:10,
      marginBottom:40,
      borderBottomColor:"green",
      borderBottomWidth:1,
      
    },

    image: {
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center"
    },

    buttonContainer: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      
      borderRadius:30,
      backgroundColor:'transparent',
      marginTop:20,
      marginLeft:-5,
    
    },

    textinput:{
        
        height:40,
        marginBottom:30,
        color:"black",
        borderBottomColor:"green",
        borderBottomWidth:1,
        
    },
    
      loginButton: {
        backgroundColor: "#9eab1d",
    
        shadowColor: "#808080",
        shadowOffset: {
          width: 0,
          height: 9,
        },
        shadowOpacity: 0.50,
        shadowRadius: 12.35,
    
        elevation: 19,
      },
   
 btnText:{
     color:"white",
 }
  });

                                        