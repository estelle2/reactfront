import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
//import axios from 'axios';
import UserList from '../components/UserList'


export default class Tab extends Component {
    state = {
      data: []
    }

    constructor(params) {
      super(params);
      this.fetchData();  
    }
  
    fetchData(){
      var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("http://192.168.1.11/depenses_api_symfony/public/api/expenses", requestOptions)
        .then(response => response.json())
        .then(result => this.setState( {data: result} ))
        .catch(error => console.log('error', error));
    }

    
  
    render() {
      return (
        <View style={styles.container}>
          <UserList data={ this.state.data } />
        </View>
      )
    }
  }
  
  const styles = StyleSheet.create({ container: { flex: 1, /*paddingTop: 20*/ } })