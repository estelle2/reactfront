import React from 'react';
/*import logo from 'https://i.imgur.com/TkIrScD.png'; */





import { StyleSheet, Text, View, Image, TouchableOpacity, ImageBackground, TextInput, Button, ScrollView } from 'react-native';

const image = { uri: "https://image.freepik.com/photos-gratuite/plante-provenant-pieces-monnaie-dans-bocal-verre-nature-floue_1150-17704.jpg" };


class Connection extends React.Component {

  state = {
    email   : '',
    password: '',
  }
  /*constructor(props) {
    super(props);
    state = {
      email   : '',
      password: '',
    }
  }


  onClickListener = (viewId) => {
    Alert.alert("Alert", "Button pressed "+viewId);
  }*/

  onLoginAuth() {
  
    let token;

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    //myHeaders.append('X-AUTH-TOKEN', monToken);

    // Récupérer email et password
    

    var raw = JSON.stringify({
      "username": this.state.email,
      "password": this.state.password
    });
    
    console.log('e', this.state.email);
    console.log('p', this.state.password);

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://192.168.1.11/depenses_api_symfony/public/api/login", requestOptions)
      .then(response => response.json())
      .then(result => {
        console.log(result.token)
        
        // Enregistrer le token globalement

        this.props.navigation.navigate('Home');
      })
      .catch(error => console.log('error', error))
  }

  render() {
    return (
      <View style={styles.container}>
      <ImageBackground source={image} style={styles.image}>
        <Text style={styles.titre}>PUSH MONEY</Text>
        <ScrollView style={styles.essai}>
      <View style={styles.inputContainer}>



          <TextInput style={styles.inputs}
              placeholder="Email"
              value={this.state.email}
              keyboardType="email-address"
              underlineColorAndroid='transparent'
              onChangeText={(newText) => this.setState({email: newText})}/>
          <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/nolan/40/000000/email.png'}}/>
        </View>



        <View style={styles.inputContainer}>
          <TextInput style={styles.inputs}
              placeholder="Password"
              value={this.state.password}
              secureTextEntry={true}
              underlineColorAndroid='transparent'
              onChangeText={(newText) => this.setState({password: newText})}
              />
              
          <Image style={styles.inputIcon} source={{uri: 'https://img.icons8.com/nolan/40/000000/key.png'}}/>
        </View>







        <TouchableOpacity style={[styles.buttonContainer, styles.loginButton]}  onPress={this.onLoginAuth.bind(this)} >
          <Text style={styles.loginText}>Connexion</Text>
        </TouchableOpacity>
        
        <TouchableOpacity style={styles.buttonContainer} onPress={() =>
              this.props.navigation.navigate('Createaccount')
            }>
            <Text style={styles.btnText}>Créer un compte</Text>
        </TouchableOpacity>
      
        </ScrollView>
       
      </ImageBackground>
    </View>
    )
  }
}
  export default Connection;
  
    
  const styles = StyleSheet.create({
   
    container: {
      flex: 1,
      flexDirection: "column",
    },
    titre: {
      color:"green",
      textAlign:"center",
      fontWeight: "bold",
      fontSize: 60,
      marginTop:100,
      marginBottom:100,
      
    },

    image: {
      flex: 1,
      resizeMode: "cover",
      justifyContent: "center"
    },
    buttonText: {
      color: "white",
      fontSize: 42,
      fontWeight: "bold",
      textAlign: "center",
      backgroundColor: "#000000a0",
      width:350,
      alignItems:"center",
      marginLeft:60,
    },
    inputContainer: {
      borderBottomColor: '#F5FCFF',
      backgroundColor: '#FFFFFF',
      borderRadius:30,
      borderBottomWidth: 1,
      width:300,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center',
      marginLeft:50,
  
      shadowColor: "#808080",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
  
      elevation: 5,
    },
    inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: '#FFFFFF',
      flex:1,
    },
    inputIcon:{
      width:30,
      height:30,
      marginRight:15,
      justifyContent: 'center'
    },
    buttonContainer: {
      height:45,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom:20,
      width:300,
      borderRadius:30,
      backgroundColor:'transparent',
      marginLeft:50,
    },
    loginButton: {
      backgroundColor: "#9eab1d",
  
      shadowColor: "#808080",
      shadowOffset: {
        width: 0,
        height: 9,
      },
      shadowOpacity: 0.50,
      shadowRadius: 12.35,
  
      elevation: 19,
    },
    loginText: {
      color: 'white',
    },
   
  });

                                        