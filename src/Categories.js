import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';

class Categories extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>categories!</Text>
        <Button
          title="Back to home"
          onPress={() =>
            this.props.navigation.navigate('Home')
          }
        />
      </View>
    );
  }
}

// ...

export default Categories;


const styles = StyleSheet.create({
   
  container: {
    flex: 1,
    flexDirection: "column"
  },

});